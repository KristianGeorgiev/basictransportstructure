package com.georgiev;

import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.DirectionsEnum;
import com.georgiev.transport.TransportFactory;
import com.georgiev.transport.air.AbstractAirTransport;
import com.georgiev.transport.air.Plane;
import com.georgiev.transport.land.AbstractLandTransport;
import com.georgiev.transport.land.Bicycle;
import com.georgiev.transport.land.Car;
import com.georgiev.transport.water.AbstractWaterTransport;
import com.georgiev.transport.water.Ship;
import com.georgiev.transport.water.Submarine;
import org.apache.log4j.Logger;
import java.util.ArrayList;

/**
 * Created by tomcat on 8/22/16.
 */
public class Service {

    private static final Logger LOGGER = Logger.getLogger(Service.class);

    private static Bicycle myBicycle = TransportFactory.createDefaultBicycle("Sprint","SX800", 10, 30);
    private static Car myCar = TransportFactory.createDefaultCar("Volkswagen", "Golf", 1280, 182);
    private static Car myCar2 = TransportFactory.createCar("BMW", "Isetta 300", 500, 85, 3);
    private static Plane myPlane= TransportFactory.createPlane("Boeing", "747",10000, 800, 15000);
    private static Ship myShip= TransportFactory.createShip("Copling", "H7000", 30000, 82, 700);
    private static Submarine mySubmarine = TransportFactory.createSubmarine("BadBomber", "5007", 35000, 96, 625);

    private static ArrayList<AbstractTransport> transports = new ArrayList<AbstractTransport>();

    static
    {
        transports.add(myBicycle);
        transports.add(myCar);
        transports.add(myCar2);
        transports.add(myPlane);
        transports.add(myShip);
        transports.add(mySubmarine);
    }


    public static void main(String[] args) {

        try {
            printAvailableTransports();

            LOGGER.info(moveBycicle(myBicycle, DirectionsEnum.LEFT));
            LOGGER.info(myShip.stop());
            LOGGER.info(mySubmarine.lower(25));
            LOGGER.info(myPlane.rise(2000));
            LOGGER.info(myPlane.rise(6));

            printStats();

        } catch (IllegalArgumentException ex){
            LOGGER.error(ex.getMessage());
        } finally {
            LOGGER.info("Program finished execution.");
        }
    }

    public static String moveBycicle(Bicycle myBicycle, DirectionsEnum direction) {
        try {
            myBicycle.turn(30, direction);
            return myBicycle.turn(30, direction);
        } catch(NullPointerException e) {
            return null;
        }
    }

    public static String stopShip(Ship myShip) {
        return myShip.stop();
    }

    public static String lowerSubmarine(Submarine mySubmarine, int meters) {
        return mySubmarine.lower(meters);
    }

    private static void printAvailableTransports() {

        for (AbstractTransport transport : transports) {
            LOGGER.info(transport);
            LOGGER.info(transport.go());
        }
    }

    private static void printStats() {

        int landTransport = 0;
        int waterTransport = 0;
        int airTransport = 0;

        if (transports.size() != 0){
            for (AbstractTransport transport : transports) {
                if (transport instanceof AbstractAirTransport){
                    airTransport ++;
                } else if (transport instanceof AbstractWaterTransport){
                    waterTransport ++;
                } else if (transport instanceof AbstractLandTransport){
                    landTransport ++;
                }
            }

            LOGGER.info(String.format("Number of land transport vehicles -> %d.", landTransport));
            LOGGER.info(String.format("Number of water transport vehicles -> %d.", waterTransport));
            LOGGER.info(String.format("Number of air transport vehicles -> %d.", airTransport));
        } else {
            LOGGER.warn("No transport units available.");
        }

    }
}
