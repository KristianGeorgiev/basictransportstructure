package com.georgiev.servlets;

import com.georgiev.transport.DirectionsEnum;
import com.georgiev.transport.TransportFactory;
import com.georgiev.transport.air.Plane;
import com.georgiev.transport.land.Bicycle;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tomcat on 9/8/16.
 */
public class WelcomeServlet extends HttpServlet {
    private RequestDispatcher jsp;
    private ServletContext context;


    @Override
    public void init(ServletConfig config) throws ServletException {
        context = config.getServletContext();
        jsp = context.getRequestDispatcher("/WEB-INF/views/welcome.jsp");
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException
    {
        jsp.forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String parameter = req.getParameter("transports");
        req.getSession().setAttribute("currentObjectType", parameter);

        if (parameter.equals("plane")) {
            resp.sendRedirect("CreateAirTransportServlet");
        } else if (parameter.equals("car")) {
            resp.sendRedirect("CreateLandTransportServlet");
        } else if (parameter.equals("bicycle")) {
            resp.sendRedirect("CreateLandTransportServlet");
        } else if (parameter.equals("ship")) {
            resp.sendRedirect("CreateWaterTransportServlet");
        } else if (parameter.equals("submarine")) {
            resp.sendRedirect("CreateWaterTransportServlet");
        }
    }
}
