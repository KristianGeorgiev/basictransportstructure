package com.georgiev.servlets.create;

import com.georgiev.transport.TransportFactory;
import com.georgiev.transport.air.AbstractAirTransport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tomcat on 9/9/16.
 */

public class CreateAirTransportServlet extends HttpServlet {
    private RequestDispatcher jsp;
    private ServletContext context;
    private String manufacturer;
    private String model;
    private long weightInKg;
    private int maxSpeedInKph;
    private int maxFlyheightInM;
    private AbstractAirTransport newTransport;

    @Override
    public void init(ServletConfig config) throws ServletException {

        context = config.getServletContext();
        jsp = context.getRequestDispatcher("/WEB-INF/views/create/createAirTransport.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        jsp.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        manufacturer = req.getParameter("manufacturer");
        model= req.getParameter("model");
        weightInKg = Long.parseLong(req.getParameter("weightInKg"));
        maxSpeedInKph = Integer.parseInt(req.getParameter("maxSpeedInKph"));
        maxFlyheightInM= Integer.parseInt(req.getParameter("maxFlyheightInM"));

        TransportFactory factory = new TransportFactory();
        String currentObjectType = (String) req.getSession().getAttribute("currentObjectType");

        if (currentObjectType.equals("plane")){
            newTransport = factory.createPlane(manufacturer, model, weightInKg, maxSpeedInKph, maxFlyheightInM);
        }

        req.getSession().setAttribute("myTransport", newTransport);

        resp.sendRedirect("VerticalMoverMethodsServlet");
    }
}
