package com.georgiev.servlets.create;

import com.georgiev.transport.TransportFactory;
import com.georgiev.transport.land.AbstractLandTransport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tomcat on 9/13/16.
 */
public class CreateLandTransportServlet extends HttpServlet{
    private RequestDispatcher jsp;
    private ServletContext context;
    private String manufacturer;
    private String model;
    private long weightInKg;
    private int maxSpeedInKph;
    private int wheelsCount;
    AbstractLandTransport newLandTransport;

    @Override
    public void init(ServletConfig config) throws ServletException {
        context = config.getServletContext();
        jsp = context.getRequestDispatcher("/WEB-INF/views/create/createLandTransport.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        jsp.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        manufacturer = req.getParameter("manufacturer");
        model= req.getParameter("model");
        weightInKg = Long.parseLong(req.getParameter("weightInKg"));
        maxSpeedInKph = Integer.parseInt(req.getParameter("maxSpeedInKph"));

        if (req.getParameter("wheelsCount") != null){
            wheelsCount = Integer.parseInt(req.getParameter("wheelsCount"));
        } else {
            wheelsCount = -1;
        }

        String currentObjectType = (String) req.getSession().getAttribute("currentObjectType");

        TransportFactory factory = new TransportFactory();

        if (currentObjectType.equals("bicycle")) {
            if (wheelsCount != -1) {
                newLandTransport = factory.createBicycle(manufacturer, model, weightInKg, maxSpeedInKph, wheelsCount);
            } else {
                newLandTransport = factory.createDefaultBicycle(manufacturer, model, weightInKg, maxSpeedInKph);
            }
        }

        if (currentObjectType.equals("car")) {
            if (wheelsCount != -1) {
                newLandTransport = factory.createCar(manufacturer, model, weightInKg, maxSpeedInKph, wheelsCount);
            } else {
                newLandTransport = factory.createDefaultCar(manufacturer, model, weightInKg, maxSpeedInKph);
            }
        }

        req.getSession().setAttribute("myTransport", newLandTransport);

        resp.sendRedirect("NonVerticalMoverMethodsServlet");
    }
}
