package com.georgiev.servlets.create;

import com.georgiev.transport.TransportFactory;
import com.georgiev.transport.water.AbstractWaterTransport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tomcat on 9/13/16.
 */
public class CreateWaterTransportServlet extends HttpServlet {
    private RequestDispatcher jsp;
    private ServletContext context;
    private String manufacturer;
    private String model;
    private long weightInKg;
    private int maxSpeedInKph;
    private int waterDisplacementInT;
    AbstractWaterTransport newTransport;

    @Override
    public void init(ServletConfig config) throws ServletException {

        context = config.getServletContext();
        jsp = context.getRequestDispatcher("/WEB-INF/views/create/createWaterTransport.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        jsp.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        manufacturer = req.getParameter("manufacturer");
        model= req.getParameter("model");
        weightInKg = Long.parseLong(req.getParameter("weightInKg"));
        maxSpeedInKph = Integer.parseInt(req.getParameter("maxSpeedInKph"));
        waterDisplacementInT = Integer.parseInt(req.getParameter("waterDisplacementInT"));

        TransportFactory factory = new TransportFactory();
        String currentObjectType = (String) req.getSession().getAttribute("currentObjectType");

        if (currentObjectType.equals("ship")){
            newTransport = factory.createShip(manufacturer, model, weightInKg, maxSpeedInKph, waterDisplacementInT);

            req.getSession().setAttribute("myTransport", newTransport);

            resp.sendRedirect("NonVerticalMoverMethodsServlet");
        } else if (currentObjectType.equals("submarine")) {
            newTransport = factory.createSubmarine(manufacturer, model, weightInKg, maxSpeedInKph, waterDisplacementInT);

            req.getSession().setAttribute("myTransport", newTransport);

            resp.sendRedirect("VerticalMoverMethodsServlet");
        }


    }
}
