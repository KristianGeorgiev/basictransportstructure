package com.georgiev.servlets.methods;

import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.land.AbstractLandTransport;
import com.georgiev.transport.water.AbstractWaterTransport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by tomcat on 9/9/16.
 */
public class NonVerticalMoverMethodsServlet extends HttpServlet {
    private RequestDispatcher jsp;
    private ServletContext context;
    private AbstractTransport currentTransport;

    @Override
    public void init(ServletConfig config) throws ServletException {
        context = config.getServletContext();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        currentTransport = (AbstractTransport) req.getSession().getAttribute("myTransport");

        if (currentTransport instanceof AbstractWaterTransport) {
            jsp = context.getRequestDispatcher("/WEB-INF/views/methods/waterTransportMethods.jsp");
        } else if (currentTransport instanceof AbstractLandTransport) {
            jsp = context.getRequestDispatcher("/WEB-INF/views/methods/landTransportMethods.jsp");
        }

        jsp.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String invokeMethod = "invokeMethod";

        HashMap<String, Object> requests = nonVerticalMoverMethods(req, resp, currentTransport, invokeMethod);

        jsp.include((HttpServletRequest)requests.get("request"), (HttpServletResponse) requests.get("response"));
    }

    protected static HashMap<String, Object> nonVerticalMoverMethods(HttpServletRequest req, HttpServletResponse resp, AbstractTransport currentTransport, String invokeMethod) throws ServletException, IOException {
        String result;
        invokeMethod = "invokeMethod";

        if (req.getParameter("go") != null) {
            result = currentTransport.go();
            req.setAttribute(invokeMethod, result);
        } else if (req.getParameter("stop") != null) {
            result = currentTransport.stop();
            req.setAttribute(invokeMethod, result);
        } else if (req.getParameter("turn") != null) {
            resp.sendRedirect("RequiredParamTurnServlet");
        } else if (req.getParameter("welcome") != null) {
            resp.sendRedirect("WelcomeServlet");
        }

        HashMap<String, Object> requests = new HashMap<String, Object>();

        requests.put("request", req);
        requests.put("response", resp);

        return requests;
    }
}
