package com.georgiev.servlets.methods;

import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.air.AbstractAirTransport;
import com.georgiev.transport.water.AbstractWaterTransport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by tomcat on 9/13/16.
 */
public class VerticalMoverMethodsServlet extends NonVerticalMoverMethodsServlet {
    private RequestDispatcher jsp;
    private ServletContext context;
    private AbstractTransport currentTransport;

    @Override
    public void init(ServletConfig config) throws ServletException {
        context = config.getServletContext();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        currentTransport = (AbstractTransport) req.getSession().getAttribute("myTransport");

        if (currentTransport instanceof AbstractWaterTransport) {
            jsp = context.getRequestDispatcher("/WEB-INF/views/methods/waterTransportMethods.jsp");
        } else if (currentTransport instanceof AbstractAirTransport) {
            jsp = context.getRequestDispatcher("/WEB-INF/views/methods/airTransportMethods.jsp");
        }

        jsp.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String invokeMethod = "invokeMethod";

        HashMap<String, Object> requests = null;

        if (req.getParameter("rise") != null) {
            req.getSession().setAttribute("riseOrLowerMethod", "rise");
            resp.sendRedirect("RequiredVerticalMoveParServlet");
        } else if (req.getParameter("lower") != null) {
            req.getSession().setAttribute("riseOrLowerMethod", "lower");
            resp.sendRedirect("RequiredVerticalMoveParServlet");
        } else {
             requests = nonVerticalMoverMethods(req, resp, currentTransport, invokeMethod);
        }

        if (requests == null){
            jsp.include(req, resp);
        } else {
            jsp.include((HttpServletRequest)requests.get("request"), (HttpServletResponse) requests.get("response"));
        }
    }
}
