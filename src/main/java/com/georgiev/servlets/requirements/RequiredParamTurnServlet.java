package com.georgiev.servlets.requirements;

import com.georgiev.servlets.methods.VerticalMoverMethodsServlet;
import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.DirectionsEnum;
import com.georgiev.transport.VerticalMover;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tomcat on 9/14/16.
 */
public class RequiredParamTurnServlet extends HttpServlet {
    private RequestDispatcher jsp;
    private ServletContext context;

    @Override
    public void init(ServletConfig config) throws ServletException {
        context = config.getServletContext();
        jsp = context.getRequestDispatcher("/WEB-INF/views/requirements/requiredParamTurn.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        jsp.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String invokeTurn;
        AbstractTransport transport = (AbstractTransport) req.getSession().getAttribute("myTransport");

        if (req.getParameter("welcome") != null) {
            resp.sendRedirect("WelcomeServlet");
        } else if (req.getParameter("goBack") != null) {
            if (transport instanceof VerticalMover) {
                resp.sendRedirect("VerticalMoverMethodsServlet");
            } else {
                resp.sendRedirect("NonVerticalMoverMethodsServlet");
            }
        } else {
            String degrees = req.getParameter("degrees");
            String directionRead = req.getParameter("direction");

            if (directionRead.equals("left")) {
                invokeTurn = transport.turn(Integer.parseInt(degrees), DirectionsEnum.LEFT);
            } else {
                invokeTurn = transport.turn(Integer.parseInt(degrees), DirectionsEnum.RIGHT);
            }

            req.setAttribute("invokeTurn", invokeTurn);

            jsp.include(req, resp);
        }
    }
}
