package com.georgiev.servlets.requirements;

import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.DirectionsEnum;
import com.georgiev.transport.VerticalMover;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tomcat on 9/14/16.
 */
public class RequiredVerticalMoveParServlet extends HttpServlet {
    private RequestDispatcher jsp;
    private ServletContext context;

    @Override
    public void init(ServletConfig config) throws ServletException {
        context = config.getServletContext();
        jsp = context.getRequestDispatcher("/WEB-INF/views/requirements/requiredParamVerticalMove.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        jsp.forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String invokeMethod;

        if (req.getParameter("welcome") != null) {
            resp.sendRedirect("WelcomeServlet");
        } else if (req.getParameter("goBack") != null) {
            resp.sendRedirect("VerticalMoverMethodsServlet");
        } else {
            String meters = req.getParameter("meters");

            VerticalMover transport = (VerticalMover) req.getSession().getAttribute("myTransport");

            String methodToInvoke = (String) req.getSession().getAttribute("riseOrLowerMethod");

            if (methodToInvoke.equals("rise")) {
                invokeMethod = transport.rise(Integer.parseInt(meters));
            } else {
                invokeMethod = transport.lower(Integer.parseInt(meters));
            }

            req.setAttribute("invokeMethod", invokeMethod);

            jsp.include(req, resp);
        }
    }
}
