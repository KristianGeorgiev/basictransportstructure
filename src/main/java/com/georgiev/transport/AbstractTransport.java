package com.georgiev.transport;

import static com.georgiev.transport.RestrictionsEnum.*;

public abstract class AbstractTransport implements Transport {
    private String model;
    private String manufacturer;
    private long weightInKg;
    private int maxSpeedInKph;

    public AbstractTransport(String model, String manufacturer, long weightInKg, int maxSpeedInKph) {
        setModel(model);
        setManufacturer(manufacturer);
        setWeightInKg(weightInKg);
        setMaxSpeedInKph(maxSpeedInKph);
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        Validator.validateStringValue(model);

        this.model = model;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        Validator.validateStringValue(manufacturer);

        this.manufacturer = manufacturer;
    }

    public long getWeightInKg() {
        return this.weightInKg;
    }

    public void setWeightInKg(long weightInKg) {

        Validator.validateMinLongValues(weightInKg, MIN_WEIGHT_KG);

        this.weightInKg = weightInKg;
    }

    public int getMaxSpeedInKph() {
        return this.maxSpeedInKph;
    }

    public void setMaxSpeedInKph(int maxSpeedInKph) {
        Validator.validateMinIntegerValues(maxSpeedInKph, MIN_SPEED_KPH);

        this.maxSpeedInKph = maxSpeedInKph;
    }

    public abstract String go();

    public String stop() {
        return String.format("%s %s stopped.", this.manufacturer, this.model);
    }

    public String turn(int degrees, DirectionsEnum direction) {
        Validator.validateMinAndMaxIntegerValues(degrees, MIN_TURN_DEGREES, MAX_TURN_DEGREES);

        return String.format("%s %s turned %d degrees %s",this.manufacturer, this.model, degrees, direction.toString().toLowerCase());
    }
}
