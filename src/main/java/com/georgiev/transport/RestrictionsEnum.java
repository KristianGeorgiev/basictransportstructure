package com.georgiev.transport;

/**
 * Created by tomcat on 8/26/16.
 */
public enum RestrictionsEnum {

    MIN_WEIGHT_KG(0),
    MIN_FLYHEIGHT_METERS(0),
    MIN_MOVING_METERS(0),
    MIN_WHEELS_COUNT(0),
    MIN_WATER_DISPLACEMENT_T(0),
    MIN_SPEED_KPH(0),
    MIN_TURN_DEGREES(0),
    MAX_TURN_DEGREES(180);


    private int value;

    RestrictionsEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
