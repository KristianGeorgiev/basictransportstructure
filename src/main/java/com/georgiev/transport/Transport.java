package com.georgiev.transport;

/**
 * Created by tomcat on 8/22/16.
 */
public interface Transport {
    String go();
    String stop();
    String turn(int degrees, DirectionsEnum direction);
}
