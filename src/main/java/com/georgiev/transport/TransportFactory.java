package com.georgiev.transport;
import com.georgiev.transport.air.Plane;
import com.georgiev.transport.land.Bicycle;
import com.georgiev.transport.land.Car;
import com.georgiev.transport.water.Ship;
import com.georgiev.transport.water.Submarine;

/**
 * Created by tomcat on 8/29/16.
 */
public class TransportFactory {

    public static Plane createPlane(String manufacturer, String model, long weightInKg, int maxSpeedInKph, int maxFlyheightInM) {
        return new Plane(model, manufacturer, weightInKg, maxSpeedInKph, maxFlyheightInM);
    }

    public static Bicycle createBicycle(String manufacturer, String model, long weightInKg, int maxSpeedInKph, int wheelsCount){
        return new Bicycle(model, manufacturer, weightInKg, maxSpeedInKph, wheelsCount);
    }

    public static Bicycle createDefaultBicycle(String manufacturer, String model, long weightInKg, int maxSpeedInKph){
        return new Bicycle(model, manufacturer, weightInKg, maxSpeedInKph);
    }

    public static Car createCar(String manufacturer, String model, long weightInKg, int maxSpeedInKph, int wheelsCount){
        return new Car(model, manufacturer, weightInKg, maxSpeedInKph, wheelsCount);

    }

    public static Car createDefaultCar(String manufacturer, String model, long weightInKg, int maxSpeedInKph){
        return new Car(model, manufacturer, weightInKg, maxSpeedInKph);
    }

    public static Ship createShip(String manufacturer, String model, long weightInKg, int maxSpeedInKph, int waterDisplacementInT){
        return new Ship(model, manufacturer, weightInKg, maxSpeedInKph, waterDisplacementInT);
    }

    public static Submarine createSubmarine(String manufacturer, String model, long weightInKg, int maxSpeedInKph, int waterDisplacementInT){
        return new Submarine(model, manufacturer, weightInKg, maxSpeedInKph, waterDisplacementInT);
    }
}
