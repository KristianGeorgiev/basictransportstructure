package com.georgiev.transport;

import static com.georgiev.transport.RestrictionsEnum.*;

/**
 * Created by tomcat on 9/5/16.
 */
public class Validator {

    //Maybe return false on every mehtod instead of exception and on setters
    // if false, then return message as string.
    // Still stops execution if wrong data is entered.

    public static boolean validateStringValue (String checkString) {
        if (checkString == null || checkString.isEmpty()) {
            throw new IllegalArgumentException("This string can not be empty or null.");
        } else {
            return true;
        }
    }

    public static boolean validateMinLongValues(Long checkLong, RestrictionsEnum restriction) {
        if (checkLong <= restriction.getValue()){
            throw new IllegalArgumentException("This value can not be negative or zero.");
        } else {
            return true;
        }
    }

    public static boolean validateMinIntegerValues(Integer checkInteger, RestrictionsEnum restriction) {
        if (checkInteger <= restriction.getValue()){
            throw new IllegalArgumentException("Value of max speed can not be negative or zero.");
        } else {
            return true;
        }
    }

    public static boolean validateMinAndMaxIntegerValues(Integer checkInt, RestrictionsEnum acceptedMin, RestrictionsEnum acceptedMax) {
        if (checkInt <= acceptedMin.getValue() || checkInt > acceptedMax.getValue()){
            throw new IllegalArgumentException(String.format("This integer should be in the range between %d and %d", acceptedMin.getValue(), acceptedMax.getValue()));
        } else {
            return true;
        }
    }
}
