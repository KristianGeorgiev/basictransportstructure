package com.georgiev.transport;

/**
 * Created by tomcat on 8/22/16.
 */
public interface VerticalMover {
    String rise(int meters);
    String lower(int meters);
}
