package com.georgiev.transport.air;

import com.georgiev.transport.Validator;
import com.georgiev.transport.VerticalMover;
import com.georgiev.transport.AbstractTransport;

import static com.georgiev.transport.RestrictionsEnum.MIN_FLYHEIGHT_METERS;
import static com.georgiev.transport.RestrictionsEnum.MIN_MOVING_METERS;

/**
 * Created by tomcat on 8/22/16.
 */
public abstract class AbstractAirTransport extends AbstractTransport implements VerticalMover {
    private int maxFlyheightInM;

    public AbstractAirTransport(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int maxFlyheightInM){
        super(model, manufacturer, weightInKg, maxSpeedInKph);

        setMaxFlyheightInM(maxFlyheightInM);
    }

    public int getMaxFlyheightInM() {
        return maxFlyheightInM;
    }

    public void setMaxFlyheightInM(int maxFlyheightInM) {
        Validator.validateMinIntegerValues(maxFlyheightInM, MIN_FLYHEIGHT_METERS);

        this.maxFlyheightInM = maxFlyheightInM;
    }

    @Override
    public String go() {
        return  String.format("%s %s flew away.", getManufacturer(), getModel());
    }

    public String rise(int meters) {
        Validator.validateMinIntegerValues(meters, MIN_MOVING_METERS);

        return String.format("%s %s rised with %d meters.", getManufacturer(), getModel(), meters);
    }

    public String lower(int meters) {
        Validator.validateMinIntegerValues(meters, MIN_MOVING_METERS);

        return String.format("%s %s lowered with %d meters.", getManufacturer(), getModel(), meters);
    }

    @Override
    public String toString() {
        return String.format("%nManufacturer: %s%nModel: %s%nWeight in kg: %d%nMax speed in kph: %d%nMax flyheight in meters: %d",
                getManufacturer(),
                getModel(),
                getWeightInKg(),
                getMaxSpeedInKph(),
                getMaxFlyheightInM());
    }
}
