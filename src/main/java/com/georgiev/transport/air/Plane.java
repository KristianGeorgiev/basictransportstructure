package com.georgiev.transport.air;

/**
 * Created by tomcat on 8/22/16.
 */
public class Plane extends AbstractAirTransport {
    public Plane(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int maxFlyheightInM){
        super(model, manufacturer, weightInKg, maxSpeedInKph, maxFlyheightInM);
    }
}
