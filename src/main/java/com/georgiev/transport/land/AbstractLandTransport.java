package com.georgiev.transport.land;

import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.Validator;

import static com.georgiev.transport.RestrictionsEnum.*;

/**
 * Created by tomcat on 8/22/16.
 */
public abstract class AbstractLandTransport extends AbstractTransport {
    private int wheelsCount;

    public AbstractLandTransport(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int wheelsCount) {
        super(model, manufacturer, weightInKg, maxSpeedInKph);

        setWheelsCount(wheelsCount);
    }

    public int getWheelsCount() {
        return this.wheelsCount;
    }

    public void setWheelsCount(int wheelsCount) {
        Validator.validateMinIntegerValues(wheelsCount, MIN_WHEELS_COUNT);

        this.wheelsCount = wheelsCount;
    }

    @Override
    public String toString() {
        String output = String.format("%nManufacturer: %s%nModel: %s%nWeight in kg: %d%nMax speed in kph: %d%nWheels count: %d",
                getManufacturer(),
                getModel(),
                getWeightInKg(),
                getMaxSpeedInKph(),
                getWheelsCount());

        return output;
    }
}
