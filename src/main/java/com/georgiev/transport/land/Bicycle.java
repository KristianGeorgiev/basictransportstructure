package com.georgiev.transport.land;

/**
 * Created by tomcat on 8/22/16.
 */
public class Bicycle extends AbstractLandTransport {

    private static final int DEFAULT_WHEELS_COUNT = 2;

    public Bicycle(String model, String manufacturer, long weightInKg, int maxSpeedInKph)
    {
        this(model, manufacturer, weightInKg, maxSpeedInKph,DEFAULT_WHEELS_COUNT);
    }

    public Bicycle(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int wheelsCount) {
        super(model, manufacturer, weightInKg, maxSpeedInKph, wheelsCount);
    }

    @Override
    public String go() {
        String result = String.format("A person rolled the pedals and %s %s started going",getManufacturer(), getModel());
        return result;
    }
}
