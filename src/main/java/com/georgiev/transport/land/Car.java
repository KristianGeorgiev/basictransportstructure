package com.georgiev.transport.land;

/**
 * Created by tomcat on 8/22/16.
 */
public class Car extends AbstractLandTransport {
    private static final int DEFAULT_WHEELS_COUNT = 4;

    public Car(String model, String manufacturer, long weightInKg, int maxSpeedInKph){
        this(model, manufacturer, weightInKg, maxSpeedInKph, DEFAULT_WHEELS_COUNT);
    }

    public Car(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int wheelsCount) {
        super(model, manufacturer, weightInKg, maxSpeedInKph, wheelsCount);
    }

    @Override
    public String go() {
        String result = String.format("A man pushed the gas pedal and %s %s started going.", getManufacturer(), getModel());
        return result;
    }
}
