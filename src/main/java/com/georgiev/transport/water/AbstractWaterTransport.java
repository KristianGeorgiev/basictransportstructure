package com.georgiev.transport.water;

import com.georgiev.transport.AbstractTransport;
import com.georgiev.transport.Validator;
import com.georgiev.transport.VerticalMover;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.georgiev.transport.RestrictionsEnum.*;

/**
 * Created by tomcat on 8/22/16.
 */
public abstract class AbstractWaterTransport extends AbstractTransport {
    private int waterDisplacementInT;

    public AbstractWaterTransport(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int waterDisplacementInT) {
        super(model, manufacturer, weightInKg, maxSpeedInKph);

        setWaterDisplacementInT(waterDisplacementInT);
    }

    public int getWaterDisplacementInT() {
        return this.waterDisplacementInT;
    }

    public void setWaterDisplacementInT(int waterDisplacementInT) {
        Validator.validateMinIntegerValues(waterDisplacementInT, MIN_WATER_DISPLACEMENT_T);
        this.waterDisplacementInT = waterDisplacementInT;
    }

    @Override
    public abstract String go();

    @Override
    public String stop() {
        return String.format("Engines started working backwards and %s %s stops.", getManufacturer(), getModel());
    }

    @Override
    public String toString() {
        String output = String.format("%nManufacturer: %s%nModel: %s%nWeight in kg: %d%nMax speed in kph: %d%nWater displacement in t: %d",
                getManufacturer(),
                getModel(),
                getWeightInKg(),
                getMaxSpeedInKph(),
                getWaterDisplacementInT());

        return output;
    }
}
