package com.georgiev.transport.water;

/**
 * Created by tomcat on 8/22/16.
 */
public class Ship extends AbstractWaterTransport {
    public Ship(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int waterDisplacementInT){
        super(model, manufacturer, weightInKg, maxSpeedInKph, waterDisplacementInT);
    }

    @Override
    public String go() {
        return String.format("Captain pushed the joystick and %s %s started going forward.", getManufacturer(), getModel());
    }
}
