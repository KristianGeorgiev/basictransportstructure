package com.georgiev.transport.water;

import com.georgiev.transport.RestrictionsEnum;
import com.georgiev.transport.Validator;
import com.georgiev.transport.VerticalMover;

/**
 * Created by tomcat on 8/22/16.
 */
public class Submarine extends AbstractWaterTransport implements VerticalMover {

    public Submarine(String model, String manufacturer, long weightInKg, int maxSpeedInKph, int waterDisplacementInT){
        super(model, manufacturer, weightInKg, maxSpeedInKph, waterDisplacementInT);
    }

    public String rise(int meters) {
        Validator.validateMinIntegerValues(meters, RestrictionsEnum.MIN_MOVING_METERS);

        return String.format("%s %s rised with %d meters.",getManufacturer(), getModel(), meters);
    }

    public String lower(int meters) {
        Validator.validateMinIntegerValues(meters, RestrictionsEnum.MIN_MOVING_METERS);

        return String.format("%s %s lowered with %d meters.",getManufacturer(), getModel(), meters);
    }


    @Override
    public String go() {
        return String.format("Captain pushed the joystick and %s %s started going forward.", getManufacturer(), getModel());
    }
}
