<!DOCTYPE html>
    <html>
        <head>
            <meta charset="US-ASCII">
             <title>Create Air Transport</title>
        </head>
        <body>

            <form action="CreateAirTransportServlet" method = "post">

                Manufacturer: <input type="text" name="manufacturer">
                </br>
                Model: <input type="text" name="model">
                </br>
                Weight in KG: <input type="text" name="weightInKg">
                </br>
                Max speed in KPH: <input type="text" name="maxSpeedInKph">
                </br>
                Max flyheight in M: <input type="text" name="maxFlyheightInM">
                </br>

                <input type="submit" value="Create">
            </form>
        </body>
    </html>