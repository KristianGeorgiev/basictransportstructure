<!DOCTYPE html>
    <html>
        <head>
            <meta charset="US-ASCII">
             <title>Create Water Transport</title>
        </head>
        <body>

            <form action="CreateWaterTransportServlet" method="post">

                Manufacturer: <input type="text" name="manufacturer">
                </br>
                Model: <input type="text" name="model">
                </br>
                Weight in KG: <input type="text" name="weightInKg">
                </br>
                Max speed in KPH: <input type="text" name="maxSpeedInKph">
                </br>
                Water displacement in T: <input type="text" name="waterDisplacementInT">

                </br>
                <input type="submit" value="Create">
            </form>
        </body>
    </html>