<%@ page isELIgnored="false" %>
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="US-ASCII">
             <title>Air Transport Methods</title>
        </head>
        <body>
            <h1>${invokeMethod}</h1>
            <h3>${myTransport.manufacturer}</h3>
            <h3>${myTransport.model}</h3>
            <h3>${myTransport.weightInKg}</h3>
            <h3>${myTransport.maxSpeedInKph}</h3>
            <h3>${myTransport.maxFlyheightInM}</h3>
            <form action="VerticalMoverMethodsServlet" method="post">
                <input type="submit" value="Go" name="go">
                <input type="submit" value="Stop" name="stop">
                <input type="submit" value="Turn" name="turn">
                <input type="submit" value="Rise" name="rise">
                <input type="submit" value="Lower" name="lower">
                <input type="submit" value="Welcome page" name="welcome">
            </form>
        </body>
    </html>