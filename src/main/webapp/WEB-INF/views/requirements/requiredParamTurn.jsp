<%@ page isELIgnored="false" %>
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="US-ASCII">
             <title>Turn Required Parameters</title>
        </head>
        <body>
            <h1>${invokeTurn}</h1>
            <form action="RequiredParamTurnServlet" method="post">

                <h3>Enter turn degrees:</h3> <input type="text" name="degrees">
                </br>
                <h3>Enter turn direction:</h3>
                </br>
                <input type="radio" name="direction" value="Left"> Left
                </br>
                <input type="radio" name="direction" value="Right"> Right
                </br>
                <input type="submit" value="Confirm">
                <input type="submit" value="Go Back" name="goBack">
                <input type="submit" value="Welcome page" name="welcome">
            </form>
        </body>
    </html>