package com.georgiev;

import com.georgiev.transport.DirectionsEnum;
import com.georgiev.transport.land.Bicycle;
import com.georgiev.transport.water.Ship;
import com.georgiev.transport.water.Submarine;
import org.easymock.EasyMock;
import org.easymock.IAnswer;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 8/31/16.
 */
public class TestService {

    private Service service;
    private Bicycle myBicycle;

    @BeforeClass
    public void setUp() {
        this.service = new Service();
//        this.myBicycle = new Bicycle("Baracuda", "Sprint", 10, 30, 3);
        myBicycle = EasyMock.createMock(Bicycle.class);

        EasyMock.expect(myBicycle.turn(EasyMock.anyInt(), EasyMock.anyObject(DirectionsEnum.class)))
                .andAnswer(new IAnswer<String>() {
                    @Override
                    public String answer() throws Throwable {
                        System.out.println("I am mock method");
                        return "Sprint Baracuda turned 30 degrees left";
                    }
                }).times(2);

        EasyMock.replay(myBicycle);
    }

    @Test
    public void testMoveBycicle() {
        String expectedResult = "Sprint Baracuda turned 30 degrees left";

        Assert.assertEquals(service.moveBycicle(myBicycle, DirectionsEnum.LEFT), expectedResult);
    }

    @Test
    public void testStopShip() {
        Ship mockedShip = EasyMock.createMock(Ship.class);

        EasyMock.expect(mockedShip.stop()).andReturn("Moched ship stopped at: 11:00:00");

        EasyMock.replay(mockedShip);

        String expectedResult = "Moched ship stopped at: 11:00:00";

        Assert.assertEquals(service.stopShip(mockedShip), expectedResult);
        EasyMock.verify(mockedShip);
    }

    @Test
    public void testLowerSubmarine() {
        Submarine mockedSubmarine = EasyMock.createMock(Submarine.class);

        EasyMock.expect(mockedSubmarine.lower(EasyMock.anyInt())).andReturn("123");

        EasyMock.replay(mockedSubmarine);

        Assert.assertEquals(service.lowerSubmarine(mockedSubmarine, 24), "123");

    }
}
