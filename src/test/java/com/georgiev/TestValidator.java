package com.georgiev;

import com.georgiev.transport.RestrictionsEnum;
import com.georgiev.transport.Validator;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 9/5/16.
 */
public class TestValidator {
    @Test
    public void testValidateStringValue() {
        Assert.assertTrue(Validator.validateStringValue("good test"));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testValidateStrValueEmptyShouldThrow() {
        Validator.validateStringValue(StringUtils.EMPTY);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testValidateStrValueNullShouldThrow() {
        Validator.validateStringValue(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testValidateMinLongValuesShouldThrow() {
        Validator.validateMinLongValues(0L, RestrictionsEnum.MIN_SPEED_KPH);
    }
}
