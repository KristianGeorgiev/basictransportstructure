package com.georgiev.transport;

import com.georgiev.transport.land.Car;
import com.georgiev.transport.water.Submarine;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 8/30/16.
 */
public class TestAbstractTransport {
    private AbstractTransport myCar;

    @BeforeClass
    public void setUp() {
        myCar = new Car("156", "Alfa Romeo", 1000, 200);
    }

    @Test
    public void testTurn() {
        String expectedValue = "Alfa Romeo 156 turned 25 degrees left";

        Assert.assertEquals(myCar.turn(25, DirectionsEnum.LEFT), expectedValue);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTurnZeroArgumentShouldThrow() {
        myCar.turn(0, DirectionsEnum.LEFT);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTurnTooBigArgumentShouldThrow() {
        myCar.turn(200, DirectionsEnum.LEFT);
    }

    @Test
    public void testStop() {

        String expectedResult = "Alfa Romeo 156 stopped.";

        Assert.assertEquals(myCar.stop(), expectedResult);
    }
}
