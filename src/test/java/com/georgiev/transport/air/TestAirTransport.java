package com.georgiev.transport.air;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 8/30/16.
 */
public class TestAirTransport {
    private AbstractAirTransport plane;

    @BeforeMethod
    public void initializeObject() {
        plane = new Plane("747", "Boeing", 10000, 700, 15000);
    }

    @Test(dataProvider = "airData")
    public void testGo(AbstractAirTransport airTransport, String expectedResult) {
              Assert.assertEquals(airTransport.go(), expectedResult);
    }
    @DataProvider
    public Object[][] airData() {
        return new Object[][] {
                {new Plane("747", "Boeing", 10000, 700, 15000), "Boeing 747 flew away."},
                {new Plane("748", "Boeing", 10000, 700, 15000), "Boeing 748 flew away."}
        };
    }

    @Test
    public void testRise() {
        String expectedResult = "Boeing 747 rised with 700 meters.";

        Assert.assertEquals(plane.rise(700), expectedResult);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testRiseShouldThrow() {
        plane.rise(0);
    }

    @Test
    public void testLower() {
        String expecterResult = "Boeing 747 lowered with 700 meters.";

        Assert.assertEquals(plane.lower(700), expecterResult);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testLowerShouldThrow() {
        plane.lower(-1);
    }

}
