package com.georgiev.transport.land;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 8/30/16.
 */
public class TestLandTransport {
    private Car car;
    private Car defaultCar;
    private Bicycle bicycle;

    @BeforeClass
    public void setUp() {
        car = new Car("i3000", "BMW", 450, 82, 3);
        bicycle = new Bicycle("Baracuda", "Sprint", 25, 35);
        defaultCar = new Car("Octavia", "Skoda", 1200, 200);
    }

    @Test(dataProvider = "setLandTrps")
    public void testGo(AbstractLandTransport landTransport, String expectedResult){
        Assert.assertEquals(landTransport.go(), expectedResult);
    }

    @DataProvider
    public Object[][] setLandTrps() {
        return new Object[][] {
                {bicycle, "A person rolled the pedals and Sprint Baracuda started going"},
                {car, "A man pushed the gas pedal and BMW i3000 started going."},
                {defaultCar, "A man pushed the gas pedal and Skoda Octavia started going."}
        };
    }
}
