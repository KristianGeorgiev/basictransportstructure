package com.georgiev.transport.water;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 8/30/16.
 */
public class TestSubmarine {
    private Submarine mySubmarine;

    @BeforeClass
    public void setUp() {
        mySubmarine = new Submarine("2590", "BadBomber", 25000, 98, 120);
    }

    @Test
    public void testLower(){
        String expectedValue = "BadBomber 2590 lowered with 10 meters.";

        Assert.assertEquals(mySubmarine.lower(10), expectedValue);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testLowerWithZeroArgumentShouldThrow() {
        mySubmarine.lower(0);
    }

    @Test
    public void testRise(){
        String expectedValue = "BadBomber 2590 rised with 10 meters.";

        Assert.assertEquals(mySubmarine.rise(10), expectedValue);
    }

    @Test(expectedExceptions =  IllegalArgumentException.class)
    public void testRiseWithZeroArgumenShouldThrow() {
        mySubmarine.rise(0);
    }
}
