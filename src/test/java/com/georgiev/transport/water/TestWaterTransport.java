package com.georgiev.transport.water;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by tomcat on 8/30/16.
 */
public class TestWaterTransport {
    private Ship myShip;
    private Submarine mySubmarine;

    @BeforeClass
    public void setUp(){
        myShip = new Ship("8500", "Copling", 15000, 75, 98);
        mySubmarine = new Submarine("2590", "BadBomber", 25000, 98, 120);
    }

    @Test(dataProvider = "setWaterTrptsToGo")
    public void testGo(AbstractWaterTransport waterTransport, String expectedValue) {
        Assert.assertEquals(waterTransport.go(), expectedValue);
    }

    @DataProvider
    public Object[][] setWaterTrptsToGo(){

        return new Object[][] {
                {myShip, "Captain pushed the joystick and Copling 8500 started going forward."},
                {mySubmarine, "Captain pushed the joystick and BadBomber 2590 started going forward."}
        };
    }

    @Test(dataProvider = "setWaterTrptsToStop")
    public void testStop(AbstractWaterTransport waterTransport, String expectedValue) {
        Assert.assertEquals(waterTransport.stop(), expectedValue);
    }

    @DataProvider
    public Object[][] setWaterTrptsToStop(){
        return new Object[][] {
                {myShip, "Engines started working backwards and Copling 8500 stops."},
                {mySubmarine, "Engines started working backwards and BadBomber 2590 stops."}
        };
    }
}
